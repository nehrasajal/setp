package com.example.step;

public class Details {

	private String m_price;
	private String m_discount;
	private String m_validDate;

	public String getPrice() {
		return m_price;
	}

	public void setPrice(String price) {
		m_price = price;
	}

	public String getDiscount() {
		return m_discount;
	}

	public void setDiscount(String discount) {
		m_discount = discount;
	}

	public String getValidDate() {
		return m_validDate;
	}

	public void setValidDate(String validDate) {
		m_validDate = validDate;
	}

}
