package com.example.step;

import java.util.List;

public class DescriptionStep {

	private List<StepData> data;

	public List<StepData> getData() {
		return data;
	}

	public void setData(List<StepData> data) {
		this.data = data;
	}

}
