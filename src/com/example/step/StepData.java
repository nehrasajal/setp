package com.example.step;

public class StepData {

	private String m_name;
	private String m_data;

	public String getName() {
		return m_name;
	}

	public void setName(String name) {
		m_name = name;
	}

	public String getData() {
		return m_data;
	}

	public void setData(String data) {
		m_data = data;
	}
}