package com.example.step;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class MainActivity extends Activity {
	Details details = new Details();
	StepData stepData1 = new StepData();
	StepData stepData2 = new StepData();
	StepData stepData3 = new StepData();
	DescriptionStep descriptionStep = new DescriptionStep();
	List<StepData> list = new ArrayList<StepData>();
	private List<EditText> values = new ArrayList<EditText>();
	private EditText value;
	
	private int mYear;
	private int mMonth;
	private int mDay;
	static final int DATE_DIALOG_ID = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		stepData1.setName("price");
		stepData1.setData("Price:");
		stepData2.setName("valid_till");
		stepData2.setData("Valid Till:");
		stepData3.setName("valid_till");
		stepData3.setData("Valid Till:");
		list.add(stepData1);
		list.add(stepData2);
		list.add(stepData3);
		descriptionStep.setData(list);
		setRows();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void setRows() {
		LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		TableLayout tl = (TableLayout) findViewById(R.id.container);
		int i =0;
		for (StepData stepData : descriptionStep.getData()) {
			TableRow tr = new TableRow(this);
			View row = inflater.inflate(R.layout.row, tr, false);
			row.setVisibility(View.VISIBLE);
			EditText value = (EditText) row.findViewById(R.id.value);
			TextView name = (TextView) row.findViewById(R.id.name);
			if (stepData.getName().equals("price")) {
				name.setText(stepData.getData());
				value.setInputType(InputType.TYPE_CLASS_NUMBER
						| InputType.TYPE_NUMBER_FLAG_DECIMAL);
			}
			if (stepData.getName().equals("discount")) {
				name.setText(stepData.getData());
				value.setInputType(InputType.TYPE_CLASS_NUMBER
						| InputType.TYPE_NUMBER_FLAG_DECIMAL);
			}
			if (stepData.getName().equals("valid_till")) {
				name.setText(stepData.getData());
				value.setInputType(InputType.TYPE_DATETIME_VARIATION_DATE);
				setDateDisplay(value);
			}
			tl.addView(row);
			values.add(value);
			i++;
		}
	}

	private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {

		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			mYear = year;
			mMonth = monthOfYear;
			mDay = dayOfMonth;
			updateDisplay();
		}
	};

	private void updateDisplay() {
		if(value != null){
		value.setText(new StringBuilder().append(mMonth + 1).append("-")
				.append(mDay).append("-").append(mYear).append(" "));
		value = null;
		}
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DATE_DIALOG_ID:
			return new DatePickerDialog(this, mDateSetListener, mYear, mMonth,
					mDay);
		}
		return null;
	}

	private void setDateDisplay(EditText value1) {
		this.value = value1;
	 value1.setOnFocusChangeListener(new View.OnFocusChangeListener() {
	
	 @Override
	 public void onFocusChange(View v, boolean hasFocus) {
		 value = (EditText)v;
	 if (hasFocus)
	 showDialog(DATE_DIALOG_ID);
	 }
	 });
	 value1.setOnClickListener(new View.OnClickListener() {
	 public void onClick(View v) {
		 value = (EditText)v;
	 showDialog(DATE_DIALOG_ID);
	 }
	 });
	
	 final Calendar c = Calendar.getInstance();
	 mYear = c.get(Calendar.YEAR);
	 mMonth = c.get(Calendar.MONTH);
	 mDay = c.get(Calendar.DAY_OF_MONTH);
	
	 updateDisplay();
	 }
}
